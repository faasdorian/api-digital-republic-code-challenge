import { Type } from 'class-transformer';
import { ArrayMaxSize, ArrayMinSize, ValidateNested } from 'class-validator';
import { Wall } from './wall.dto';

export class CalculateRoomRequest {
  @ArrayMaxSize(4, { message: 'Room must have 4 walls.' })
  @ArrayMinSize(4, { message: 'Room must have 4 walls.' })
  @ValidateNested({ each: true })
  @Type(() => Wall)
  walls: Wall[];
}
