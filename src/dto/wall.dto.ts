import { IsDecimal, IsInt, IsOptional, IsPositive, Min } from 'class-validator';

export class Wall {
  @IsPositive()
  width: number;

  @IsPositive()
  height: number;

  @IsOptional()
  @IsInt()
  doors: number = 0;

  @IsOptional()
  @IsInt()
  windows: number = 0;
}
