import { Module } from '@nestjs/common';
import { CalculateControllerV1 } from './controller/v1/calculate.controller';
import { CalculateService } from './service/calculate.service';

@Module({
  imports: [],
  controllers: [CalculateControllerV1],
  providers: [CalculateService],
})
export class AppModule {}
