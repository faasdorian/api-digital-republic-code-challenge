import { Body, Controller, Post } from '@nestjs/common';
import { CalculateRoomRequest } from 'src/dto/calculate-room-request.dto';
import { CalculateService } from '../../service/calculate.service';

@Controller({ version: '1', path: 'calculate' })
export class CalculateControllerV1 {
  constructor(private readonly calculateService: CalculateService) {}

  @Post('room')
  calculateRoom(@Body() calculateRoomRequest: CalculateRoomRequest): number[] {
    return this.calculateService.calculateRoom(calculateRoomRequest);
  }
}
