import { BadRequestException, Injectable } from '@nestjs/common';
import { CalculateRoomRequest } from 'src/dto/calculate-room-request.dto';

@Injectable()
export class CalculateService {
  PAINT_CANS = [18, 3.6, 2.5, 0.5];
  DOOR_SIZE = [0.8, 1.9];
  WINDOW_SIZE = [2.0, 1.2];

  calculateRoom(calculateRoomRequest: CalculateRoomRequest): number[] {
    var totalWallArea = 0;
    calculateRoomRequest.walls.forEach((wall) => {
      const wallArea = wall.width * wall.height;
      if (wallArea < 1 || wallArea > 50)
        throw new BadRequestException('Wall area must be between 1 and 50 meters.');
      if (wall.doors > 0 && wall.height < this.DOOR_SIZE[1] + 0.3)
        throw new BadRequestException(
          `Wall must be 30cm higher than door height (${this.DOOR_SIZE[1]})`
        );

      const doorsAndWindowsArea =
        (wall.doors ??= 0) * (this.DOOR_SIZE[0] * this.DOOR_SIZE[1]) +
        (wall.windows ??= 0) * (this.WINDOW_SIZE[0] * this.WINDOW_SIZE[1]);
      if (doorsAndWindowsArea / wallArea > 0.5)
        throw new BadRequestException(
          'Total area of doors and windows must be less than 50% of wall area.'
        );

      totalWallArea += wallArea - doorsAndWindowsArea;
    });
    return this.calculatePaintCans(totalWallArea);
  }

  calculatePaintCans(wallArea: number): number[] {
    var requiredLiters = wallArea / 5;
    const requiredPaintCans: number[] = [];
    this.PAINT_CANS.forEach((paintCan) => {
      while (paintCan <= requiredLiters) {
        requiredLiters -= paintCan;
        requiredPaintCans.push(paintCan);
      }
    });
    return requiredPaintCans;
  }
}
