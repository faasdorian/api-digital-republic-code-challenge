# api-digital-republic-code-challenge


## Installation

```bash
$ npm install
```

## Running the app

```bash
$ npm run start
```

## Paths
```
POST localhost:3000/api/v1/calculate/room
```
Body example:
``` JSON
{
    "walls": [
        {
            "width": 10.0,
            "height": 2.2,
            "doors": 1,
            "windows": 2
        },
        {
            "width": 13.0,
            "height": 3.0,
            "windows": 1
        },
        {
            "width": 13.0,
            "height": 3.0
        },
        {
            "width": 13.0,
            "height": 3.0
        }
    ]
}
```
